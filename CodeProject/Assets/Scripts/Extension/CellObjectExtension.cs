using System.Collections;
using System.Collections.Generic;

public static class CellObjectExtension
{
    public static void ClearColor(this List<CellObject> ACell)
    {
        ACell.ToArray().ClearColor();
    }
    public static void ClearColor(this CellObject[] ACell)
    {
        foreach (CellObject obj in ACell)
        {
            obj.ClearColor();
        }
    }

    public static void ClearColor(this List<CellInfo> ACell)
    {
        ACell.ToArray().ClearColor();
    }
    public static void ClearColor(this CellInfo[] ACell)
    {
        foreach (CellInfo obj in ACell)
        {
            ((CellObject)obj.cellObject).ClearColor();
        }
    }
}
