using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LinesDrawer : MonoBehaviour
{
    private const int CLEAR_POINTS = 0;

    private LineRenderer lineRenderer;

    private void Start()
    {
        this.lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if (lineRenderer.positionCount > 0)
        {
            Vector3 mousePosition = Input.mousePosition;

            lineRenderer.SetPosition(lineRenderer.positionCount - 1, mousePosition);
        }
    }

    public void AddPoint(Vector3 point)
    {
        lineRenderer.positionCount++;
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, point);
    }
    public void ClearPoints()
    {
        lineRenderer.positionCount = CLEAR_POINTS;
    }
}
