using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResultInfo
{
    public string resultText;
    public Color resultColor;
}
