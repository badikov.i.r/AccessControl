using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public enum Result
{
    Granted,
    Denied
}

public class Resulter : MonoBehaviour
{
    [SerializeField]
    private Text theResultText;

    [SerializeField]
    private ResultInfo granted;
    [SerializeField]
    private ResultInfo denied;

    public void SetResult(Result theResult)
    {
        switch (theResult)
        {
            case Result.Granted:
                ChangeText(granted);
                break;
            case Result.Denied:
                ChangeText(denied);
                break;
        }
    }

    private void ChangeText(string message, Color theTextColor)
    {
        theResultText.text = message;
        theResultText.color = theTextColor;
    }
    private void ChangeText(ResultInfo result)
    {
        ChangeText(result.resultText, result.resultColor);
    }
}
