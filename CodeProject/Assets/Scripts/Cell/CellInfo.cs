using UnityEngine;

[System.Serializable]
public class CellInfo
{
    private static int countIndex;

    public int cellIndex;
    [HideInInspector]
    public MonoBehaviour cellObject;

    public CellInfo(MonoBehaviour cellObject)
    {
        this.cellObject = cellObject;
    }
    public CellInfo(MonoBehaviour cellObject, int customIndex) : this(cellObject)
    {
        this.cellIndex = customIndex;
    }

#if UNITY_EDITOR
    public void UpdateIndex()
    {
        cellIndex = ++countIndex;
    }

    public static void Putin()
    {
        countIndex = 0;
    }
#endif
}