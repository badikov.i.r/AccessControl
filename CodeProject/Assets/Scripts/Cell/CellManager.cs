using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Resulter))]
public class CellManager : MonoBehaviour
{
    [SerializeField]
    private Resulter theResulter;

    [SerializeField]
    private List<CellInfo> ethalon;
    private List<CellInfo> enteredCells;

    private void Start()
    {
        theResulter = GetComponent<Resulter>();
        enteredCells = new List<CellInfo>();
    }

    public bool AddCell(CellInfo theCell)
    {
        foreach (CellInfo currCell in enteredCells)
        {
            if (currCell.cellIndex == theCell.cellIndex)
            {
                return false;
            }
        }

        enteredCells.Add(theCell);

        return true;
    }

    public void CheckEntered()
    {
        if (CompareCells(enteredCells.ToArray(), ethalon.ToArray()))
        {
            theResulter.SetResult(Result.Granted);
        }
        else
        {
            theResulter.SetResult(Result.Denied);
        }
    }

    private bool CompareCells(CellInfo[] input, CellInfo[] toCompareWith)
    {
        if ((input.Length == 0 || toCompareWith.Length == 0)
           || input.Length != toCompareWith.Length)
        {
            Debug.LogError("Error in input or ethalon");
            Debug.Log("Ethalon count: " + toCompareWith.Length);
            Debug.Log("Input count: " + input.Length);

            ClearCells();

            return false;
        }

        for (int pos = 0; pos < input.Length; pos++)
        {
            if (input[pos].cellIndex != toCompareWith[pos].cellIndex)
            {
                Debug.Log("Not matching");
                ClearCells();

                return false;
            }
        }

        Debug.Log("Access granted");

        ClearCells();

        // if entered code is not valid
        return true;
    }

    private void ClearCells()
    {
        enteredCells.ClearColor();
        enteredCells.Clear();
    }

#if UNITY_EDITOR

    [SerializeField]
    private GameObject cellHolder;
    [SerializeField]
    private List<CellInfo> cells;

    [SerializeField]
    private bool putinCells;

    private void OnValidate()
    {
        if (putinCells)
        {
            Obnulenie();

            putinCells = false;
        }

        ShowCells();
    }

    private void Obnulenie() {
        CellInfo.Putin();
    }

    private void ShowCells()
    {
        CellObject[] sceneCells = cellHolder.GetComponentsInChildren<CellObject>();

        cells.Clear();

        foreach (CellObject cell in sceneCells)
        {
            cells.Add(cell.Cell);
        }
    }

#endif
}
