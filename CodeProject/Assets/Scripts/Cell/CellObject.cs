using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CellObject : MonoBehaviour
{
    private static bool isDragged;

    [Header("Cell info")]
    [SerializeField]
    private CellInfo currentCell;
    public CellInfo Cell
    {
        get { return currentCell; }
        private set { }
    }

    private CellManager cellManager;
    private LinesDrawer linesDrawer;

    [Header("UI states")]
    private Color standardColor;
    [SerializeField]
    private Color hoverColor;

    private void Start()
    {
        cellManager = FindObjectOfType<CellManager>();
        linesDrawer = FindObjectOfType<LinesDrawer>();

        standardColor = GetComponent<RawImage>().color;

        isDragged = false;

        currentCell.cellObject = this;

        GetComponentInChildren<Text>().text = currentCell.cellIndex.ToString();
    }

    public void OnDragStart(BaseEventData data)
    {
        isDragged = true;

        OnHover(data);
    }
    public void OnDragEnd(BaseEventData data)
    {
        isDragged = false;

        SetColor(standardColor);

        cellManager.CheckEntered();
        linesDrawer.ClearPoints();
    }
    public void OnHover(BaseEventData data)
    {
        if (!isDragged) return;

        SetColor(hoverColor);

        if (AddCell())
        {
            linesDrawer.AddPoint(transform.position);
        }
    }

    private bool AddCell()
    {
        Debug.Log("Added cell " + currentCell.cellIndex);

        return cellManager.AddCell(currentCell);
    }

    private void SetColor(Color newColor)
    {
        GetComponent<RawImage>().color = newColor;
    }

    public void ClearColor()
    {
        SetColor(standardColor);
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        GetComponentInChildren<Text>().text = currentCell.cellIndex.ToString();
        standardColor = GetComponent<RawImage>().color;
    }
#endif
}
